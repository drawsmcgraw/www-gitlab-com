---
layout: handbook-page-toc
title: Training
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Training page! This page is our SSOT page for all training items. 

### **Legal**

*  Add list here 

### **Finance**

*  N/A

### **People**

*  Learning Sessions
   *  [Live Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning) Sessions 
      *  [Ally Training](https://about.gitlab.com/company/culture/inclusion/ally-training/)
      *  [Inclusion Training](https://about.gitlab.com/company/culture/inclusion/inclusion-training/)
      *  [Receiving Feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/#responding-to-feedback)
      *  [Communicating Effectively & Responsibly Through Text](https://about.gitlab.com/company/culture/all-remote/effective-communication/)
      *  [Compensation Review: Manager Cycle (Compaas)](https://www.youtube.com/watch?v=crkPeOjkqTQ&feature=youtu.be) - just a youtube video
   * [Action Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/#action-learning) 
   * [Leadership Forum](https://about.gitlab.com/handbook/people-group/learning-and-development/leadership-forum/)  
*  [Certifications & Knowledge Assessments](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/)
   *  [Values](https://about.gitlab.com/handbook/values/#gitlab-values-certification)
   *  [Communication](https://about.gitlab.com/handbook/communication/#communication-certification)
   *  [Ally](https://about.gitlab.com/company/culture/inclusion/ally-training/#ally-certification)
   *  [Inclusion](https://about.gitlab.com/company/culture/inclusion/inclusion-training/#inclusion-certification)
   *  [GitLab 101](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-101/)
*  [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)
*  [Anti-Harassment](https://about.gitlab.com/handbook/people-group/learning-and-development/#common-ground-harassment-prevention-training) 
*  [New Manager Enablement](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-development/)
* [People Specialist team and People Experience team training issue](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* [People Specialist team - Contractor Conversion Starter Kit](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* Onboarding/Training Issues
   *  [Onboarding Issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md)
   *  [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/interview_training.md)
   *  [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)

### **Engineering**

*  Add list here 

### **Product**

*  Add list here 

### **Marketing**

*  Add list here

### **Field Sales/Sales Enablement**

*  Add list here 

### **Professional Services**

*  Add list here 
